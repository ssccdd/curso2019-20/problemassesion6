[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 6

Problemas propuestos para la Sesión 6 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

En esta sesión de prácticas se proponen ejercicio para trabajar con el nuevo **marco de ejecución Fork/Join**. Para ello utilizaremos las clases [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html), para el **marco de ejecución**, y [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html), para las tareas que se ejecutarán. Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion6#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion6#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion6#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion6#grupo-4)

## Grupo 1

El ejercicio consiste en realizar una tarea que nos calcule el inventario de un almacén para un número de componentes generados aleatoriamente. Para la realización de la tarea de cálculo definiremos una clase que hereda de [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. Para la solución se tienen que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Componente`: Clase que representa a un componente de un `TipoComponente` de los que estarán en el almacén y que se tendrán que contabilizar para el cálculo del inventario.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `Inventario`: Esta clase será el elemento que calcula la tarea `CalcularInventario`. Debe tener un contador para cada uno de los elementos presentes en `TipoComponente`. Su diseño debe contemplar la posibilidad de cambios en `TipoComponente`, es decir, no debe ser necesaria un cambio en su diseño si cambia el diseño de `TipoComponente`. Se deben definir los métodos necesarios para la gestión del inventario de cada uno de esos componentes. También debe implementarse el método `toString()` para dar una representación imprimible de un objeto de inventario.

- `CalcularInventario`: En su construcción se le debe pasar el almacén de los componentes y el inicio y fin de los elementos del almacén para realizar su cálculo de inventario.
	- Si el número de elementos para la realización del cálculo de inventario supera el valor de la constante `VALOR_BASE`:
		- Se generan dos nuevas tareas que deben realizar el cálculo del inventario para la mitad de los elementos que tiene la tarea padre.
		- Se debe esperar a la finalización de las tareas hijas antes de mezclar sus resultados de inventario.
	- Si el número de elementos es menor o igual que el valor de la constante `VALOR_BASE`:
		- Se recorren los elementos del almacén asignados y se calcula el inventario para cada uno de los `TipoComponente` que estén en el almacén asignado.
	- A la finalización de la tarea debe devolver el cálculo de inventario del almacén que le ha sido asignado.

- `Hilo principal`: Realizará los siguientes pasos:
	- Crear el marco de ejecución `ForkJoinPool`.
	- Crear un almacén con capacidad con el valor de la constante `INVENTARIO`. Los componentes deben generarse aleatoriamente.
	- Crear una tarea `CalcularInventario` que se añadirá al **marco Fork/Join**.
	- Se espera hasta que se obtiene el cálculo de `Inventario` antes de presentar el resultado y finalizar.

## Grupo 2

En una granja de rendering (render farm) se generan fotogramas que luego se renderizan. Los fotogramas tienen una capacidad gráfica **BASICA**, **MEDIA** o **ALTA**.

Construye una utilidad que dado un array de fotogramas, pueda calcular de forma concurrente cuantos de ellos hay de cada tipo de capacidad.

Para la realización de la tarea de cálculo definiremos una clase que hereda de `RecursiveTask` que se ejecutará en un  marco **Fork/Join (ForkJoinPool)**. Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. Para la solución se tienen que utilizar los siguientes elementos ya programados:

- `Constantes`: Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Fotograma`: Son los elementos del array sobre los que hay que hacer el cálculo.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `Info`: Es la clase que contiene el resultado del cálculo. Es lo que devuelve la clase `CalcularCapacidades`. Consiste en un array de enteros, con un elemento por cada capacidad gráfica y que tiene el número de veces que se ha encontrado un `Fotograma` con esa capacidad gráfica.

- `CalcularCapacidades`: Es la clase que hace la tarea de buscar en el array de `Fotograma` cuántos tiene cada una de las capacidades gráficas posible. Lo debe hacer de forma recursiva concurrente. Para eso debe implementar la interfaz `RecursiveTask`. Su funcionamiento es:

    - Al ejecutarse tiene que comprobar si el tamaño del espacio de búsqueda es mayor que el valor presente en `VALOR_BASE`:
        En caso afirmativo:
        - Dividir el espacio de búsqueda a la mitad y le asociamos una tarea a cada uno de esos subespacios de búsqueda.
        - Añadir las tareas al marco Fork/Join y esperar a tener el resultado de ambas tareas.
        - Componer el resultado de la tarea como la mezcla de los dos resultados obtenidos.
        - Devolver ese resultado calculado.

        En caso negativo:
        - Realizamos el cálculo del resultado contanto cuantos fotogramas dentro del espacio de búsqueda que tiene asignada la tarea tiene cada una de las capacidades gráficas y anotando esos datos en en resultado.
        - Devolver ese resultado calculado.

- `Hilo principal`: Realizará los siguientes pasos:

    - Crear el marco de ejecución `ForkJoinPool`.
    - Crear un array de `Fotograma` (el espacio de búsqueda inicial) con una capacidad de `TAMANIO_FOTOGRAMAS`. Los fotogramas se generan aleatoriamente.
    - Crear una tarea `CalcularCapacidades` y añadirla al marco `Fork/Join`.
    - Esperar hasta que se obtiene el cálculo de `Info` antes de presentar el resultado y finalizar.

## Grupo 3

El ejercicio consiste en realizar una tarea que nos busque un `TipoProceso` en un espacio de búsqueda. Deseamos encontrar un mínimo de `OCURRENCIAS` y localizar la posición en el espacio de búsqueda para todas las ocurrencias encontradas. Los `Proceso` se generarán aleatoriamente para conformar el espacio de búsqueda, al igual que el `TipoProceso` que se busca. Para la realización de la tarea de cálculo definiremos una clase que hereda de [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. Para la solución se tienen que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que conformará el espacio de búsqueda y así localizar su posición atendiendo al `TipoProceso` asociado.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `Resultado`: Esta clase será el elemento que calcula la tarea `BuscarProceso`. Debe tener el `TipoProceso` que se está buscando para obtener su información asociada. Su diseño debe contemplar la posibilidad de cambios en `TipoProceso`, es decir, no debe ser necesaria un cambio en su diseño si cambia el diseño de `TipoProceso`. Además deberá tener una lista que permita almacenar las posiciones donde se encuentra el `TipoProceso` buscado. Se deben definir los métodos necesarios para la gestión del resultado por la tarea `BuscarProceso`. También debe implementarse el método `toString()` para dar una representación imprimible para un objeto resultado. Deberá dejar claro si se ha encontrado el número de `OCURRENCIAS` que viene definido en `Constantes`. 

- `BuscarProceso`: Tiene que tener el espacio de búsqueda de los procesos a localizar, el inicio y el final de ese espacio de búsqueda asociado a la tarea. Además el `TipoProceso` que se está buscando.
	- Al ejecutarse tiene que comprobar si el tamaño del espacio de búsqueda es mayor que el valor presente en `VALOR_BASE`:
		- En caso afirmativo
			- Dividimos el espacio de búsqueda a la mitad y le asociamos una tarea a cada uno de esos espacios de búsqueda.
			- Añadimos las tareas al **marco Fork/Join** y esperamos a tener el resultado de ambas tareas.
			- Componemos el resultado de la tarea como la mezcla de los dos resultados obtenidos.
			- Se devuelve ese resultado calculado.
		- En caso negativo
			- Realizamos el cálculo del resultado obteniendo todas las posiciones donde se encuentra un `Proceso` que tenga asociado el `TipoProceso` que se está buscando.
			- Se devuelve ese resultado calculado.

- `Hilo principal`: Realizará los siguientes pasos:
	- Crear el marco de ejecución `ForkJoinPool`.
	- Crear un espacio de búsqueda con una capacidad expresada en la constante `TOTAL_PROCESOS`. Los procesos deben generarse aleatoriamente.
	- Crear una tarea `BuscarProceso` que buscará un `TipoProceso` generado aleatoriamente y se añadirá al **marco Fork/Join**.
	- Se espera hasta que se obtiene el cálculo de `Resultado` antes de presentar el resultado y finalizar.

## Grupo 4

El ejercicio consiste en realizar una tarea que nos calcule el inventario de un almacén para un número de sensores generados aleatoriamente. Para la realización de la tarea de cálculo definiremos una clase que hereda de  [`RecursiveAction`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Para la solución se tienen que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor que forma parte del inventario que el operario deberá calcular.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `Inventario`: Esta clase será el elemento que calcula la tarea `CalcularInventario` y hay que garantizar que esas tareas aceden en **exclusion mutua** al objeto de `Inventario`. Debe tener un contador para cada uno de los elementos presentes en `TipoSensor`. Su diseño debe contemplar la posibilidad de cambios en `TipoSensor`, es decir, no debe ser necesaria un cambio en su diseño si cambia el diseño de `TipoSensor`. Se deben definir los métodos necesarios para la gestión del inventario de cada uno de esos componentes. También debe implementarse el método `toString()` para dar una representación imprimible de un objeto de inventario.

- `CalcularInventario`: En su construcción se le debe pasar el almacén de los sensores y el inicio y fin de los elementos del almacén sobre el que se realiza el inventario. El objeto de `Inventario` también debe pasarse en el constructor para que lo compartan todas las tareas `CalcularInventaro`.
	- Si el número de elementos para la realización del cálculo de inventario supera el valor de la constante `VALOR_BASE`:
		- Se generan dos nuevas tareas que deben realizar el cálculo del inventario para la mitad de los elementos que tiene la tarea padre.
		- Se debe esperar a la finalización de las tareas hijas antes poder finalizar.
	- Si el número de elementos es menor o igual que el valor de la constante `VALOR_BASE`:
		- Se recorren los elementos del almacén asignados y se calcula el inventario para cada uno de los `TipoSensor` que estén en el almacén asignado.
		- El acceso al objeto de Inventario debe hacerse en **exclusion mutua**.
- `Hilo principal`: Realizará los siguientes pasos:
	- Crear el marco de ejecución `ForkJoinPool`.
	- Crear un almacén con capacidad con el valor de la constante `INVENTARIO`. Los sensores deben generarse aleatoriamente.
	- Crear el objeto de `Inventario` que debe calcular la tarea `CalcularInventario`.
	- Crear una tarea `CalcularInventario` que se añadirá al **marco Fork/Join**.
	- Se espera hasta que se obtiene el cálculo de `Inventario` antes de presentar el resultado y finalizar.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI2NDkzNDA0MywtMTE2Mzk1ODk1MywtOD
AzMTEwNTk1XX0=
-->