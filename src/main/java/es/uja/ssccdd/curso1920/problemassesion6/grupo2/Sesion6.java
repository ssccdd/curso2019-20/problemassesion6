/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo2.Constantes.TAMANIO_FOTOGRAMAS;
import es.uja.ssccdd.curso1920.problemassesion6.grupo2.Constantes.TipoCapacidadGrafica;
import java.util.concurrent.ForkJoinPool;

/**
 *
 * @author pedroj
 */
public class Sesion6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fotograma[] fotogramas;
        Info info;
        ForkJoinPool poolEjecucion;
        CalcularCapacidades calculador;
        
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecucion\n");
        
        // - Crear un nuevo marco de ejecucion Fork/Join
        poolEjecucion = new ForkJoinPool();
        
        // - Construir los fotogramas que hay que procesar
        fotogramas = new Fotograma[TAMANIO_FOTOGRAMAS];
        for (int i=0; i<TAMANIO_FOTOGRAMAS; i++) {
            fotogramas[i] = new Fotograma(i, TipoCapacidadGrafica.getCapacidad());
        }
        
        // - Crear la tarea recursiva y ejecutarla esperando a que termine su
        //   ejecucion
        calculador = new CalcularCapacidades(fotogramas, 0, TAMANIO_FOTOGRAMAS);
        info = poolEjecucion.invoke(calculador);
        poolEjecucion.shutdown();
        
        // - Imprimir el resultado
        System.out.println(info);
        
        System.out.println("** Hilo(PRINCIPAL): Ha terminado la ejecucion\n");
    }
    
}
