/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo1.Constantes.COMPONENTES;
import es.uja.ssccdd.curso1920.problemassesion6.grupo1.Constantes.TipoComponente;

/**
 *
 * @author pedroj
 */
public class Inventario {
    private final int[] inventario;

    /**
     * Inicializa el inventario para cada uno de los componentes disponibles
     * para la fabricación de un ordenador
     */
    public Inventario() {
        this.inventario = new int[COMPONENTES.length];
        for( int i = 0; i < COMPONENTES.length; i++ )
            inventario[i] = 0;
    }
    
    public void add(TipoComponente componente) {
        inventario[componente.ordinal()]++;
    }
    
    public void add(TipoComponente componente, int cantidad) {
        inventario[componente.ordinal()] += cantidad;
    }
    
    public void drop(TipoComponente componente) {
        inventario[componente.ordinal()]--;
    }
    
    public void drop(TipoComponente componente, int cantidad) {
        inventario[componente.ordinal()] -= cantidad;
    }
    
    public int getInventario(TipoComponente componente) {
        return inventario[componente.ordinal()];
    }

    @Override
    public String toString() {
        String salida = "Inventario{ Componentes\n\t";
        
        for( TipoComponente componente : COMPONENTES )
            salida = salida + componente + " = " 
                            + inventario[componente.ordinal()] + " unidades\n\t";
        
        return salida + "}";
    }
}
