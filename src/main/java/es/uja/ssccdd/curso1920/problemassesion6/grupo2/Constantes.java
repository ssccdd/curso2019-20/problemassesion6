/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo2;

import java.util.Random;

/**
 * Constantes, tipo enumerado y generador de numeros aleatorios necesarios para
 * el problema.
 * @author fconde
 */
public class Constantes {
    
    // Generador aleatorio
    public static final Random aleatorio = new Random();

    // Enumerado para el tipo de necesidades de un fotograma / capacidades de
    // un ordenador
    public enum TipoCapacidadGrafica {
        BASICAS(10, 3), MEDIAS(20, 5), AVANZADAS(30, 7);
        
        private final int valor;
        // - Lo que se tarda en asignar un fotograma con estas necesidades a
        //   un ordenador
        private final int tiempo;

        private TipoCapacidadGrafica(int valor, int tiempo) {
            this.valor = valor;
            this.tiempo = tiempo;
        }
        
        /**
         * Obtenemos una capacidad/necesiad grafica aleatoriamente. Tal y como
         * se ha diseñado, generara mas capacidades avanzadas, que medias y que
         * basicas. No es un error, se ha querido que sea asi.
         * @return el TipoCapacidadGrafica aleatoria
         */
        public static TipoCapacidadGrafica getCapacidad() {
            int valor = aleatorio.nextInt(MAX_VALOR_CAPACIDAD);
            TipoCapacidadGrafica resultado = null;
            TipoCapacidadGrafica[] capacidades = TipoCapacidadGrafica.values();
            int i = 0;
            
            while( (i < capacidades.length) && (resultado == null) ) {
                if ( capacidades[i].valor >= valor )
                    resultado = capacidades[i];
                
                i++;
            }
            
            return resultado;
        } 
        
        /**
         * Calcula el tiempo de asignacion para un fotograma, con una variacion
         * aleatoria
         * @return 
         *      el tiempo de asignacion para ese sensor
         */
        public int tiempoAsignacion() {
            return aleatorio.nextInt(VARIACION) + tiempo;
        }

        @Override
        public String toString() {
            return "TipoCapacidadGrafica{" + this.name() + ", valor=" + valor + '}';
        }  
    }

    // Constantes 
    public static final int MAX_VALOR_CAPACIDAD = 31;
    public static final int VARIACION = 3;
    public static final int VALOR_BASE = 16;
    public static final int TAMANIO_FOTOGRAMAS = 1024;
}
