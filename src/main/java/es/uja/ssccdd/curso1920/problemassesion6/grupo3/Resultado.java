/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.OCURRENCIAS;
import es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.TipoProceso;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Resultado {
    private final TipoProceso valor;
    private final ArrayList<Integer> posicion;

    public Resultado(TipoProceso valor) {
        this.valor = valor;
        this.posicion = new ArrayList();
    }
    
    public void addPosicion(int valor) {
        posicion.add(valor);
    }

    public TipoProceso getValor() {
        return valor;
    }

    public ArrayList<Integer> getPosicion() {
        return posicion;
    }
    
    public boolean encontrado() {
        return posicion.size() >= OCURRENCIAS;
    }

    @Override
    public String toString() {
        String resultado = "Resultado{";
        
        if( encontrado() )
            resultado += "\n\tTipo Proceso " + valor + "\n\tPosiciones\n\t" 
                         + posicion + "\n\t}" + "\n\tOcurrencias mínimas " + OCURRENCIAS +
                         "\n\tOcurrencias encotradas " + posicion.size();
        else
            resultado += " NO ENCONTRADO }" + "\n\tTipo Proceso " + valor +
                         "\n\tOcurrencias mínimas " + OCURRENCIAS +
                         "\n\tOcurrencias encotradas " + posicion.size();
        
        return resultado;
    }
}
