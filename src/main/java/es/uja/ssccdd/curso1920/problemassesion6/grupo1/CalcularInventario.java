/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo1.Constantes.COMPONENTES;
import es.uja.ssccdd.curso1920.problemassesion6.grupo1.Constantes.TipoComponente;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo1.Constantes.VALOR_BASE;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author pedroj
 */
public class CalcularInventario extends RecursiveTask<Inventario> {
    private final Componente[] almacen;
    private final int inicio;
    private final int fin;
    private final Inventario inventario;

    public CalcularInventario(Componente[] almacen, int inicio, int fin) {
        this.almacen = almacen;
        this.inicio = inicio;
        this.fin = fin;
        this.inventario = new Inventario();
    }
    
    @Override
    protected Inventario compute() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                           " El espacio asignado del almacen, inicio " + inicio +
                           " fin " + fin);
        // Comprobamos si estamos en el caso base
        if( (fin - inicio) > VALOR_BASE ) {
            // Dividimos el problema a la mitad y esperamos a que finalicen
            int mitad = (inicio + fin) / 2;
            CalcularInventario tarea1 = new CalcularInventario(almacen, inicio, mitad);
            CalcularInventario tarea2 = new CalcularInventario(almacen, mitad, fin);
            invokeAll(tarea1,tarea2);
            
            try {
                // Mezclamos los resultados
                mezclar(tarea1.get(), tarea2.get());
            } catch (InterruptedException | ExecutionException ex) {
                System.out.println("HILO(" + Thread.currentThread().getName()+
                                   ") ha sido CANCELADO" );
            }
        } else {
            resolver();
        }
        
        return inventario;
    }
    
    private void resolver() {
        for( int i = inicio; i < fin; i++ )
            inventario.add(almacen[i].getComponente());
    }
    
    private void mezclar(Inventario valor1, Inventario valor2) {
        for( TipoComponente componente : COMPONENTES ) {
            inventario.add(componente, valor1.getInventario(componente));
            inventario.add(componente, valor2.getInventario(componente));
        }
    }
}
