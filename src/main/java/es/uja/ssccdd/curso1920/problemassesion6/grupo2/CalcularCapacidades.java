/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo2;

import es.uja.ssccdd.curso1920.problemassesion6.grupo2.Constantes.TipoCapacidadGrafica;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.VALOR_BASE;
import java.util.concurrent.ExecutionException;
import static java.util.concurrent.ForkJoinTask.invokeAll;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que calcula el numero de fotogramas que tienen cada una de las capacidades
 * graficas posibles.
 * @author fconde
 */
public class CalcularCapacidades extends RecursiveTask<Info> {
    
    private final Fotograma[] fotogramas;
    private final int inicio;
    private final int fin;
    private final Info resultado;
    
    public CalcularCapacidades(Fotograma[] fotogramas, int inicio, int fin) {
        this.fotogramas = fotogramas;
        this.inicio = inicio;
        this.fin = fin;
        this.resultado = new Info();
    }

    @Override
    protected Info compute() {
        if ((fin-inicio) > VALOR_BASE) {
            int mitad = (inicio+fin)/2;
            CalcularCapacidades subTareaA = new CalcularCapacidades(fotogramas, inicio, mitad);
            CalcularCapacidades subTareaB = new CalcularCapacidades(fotogramas, mitad, fin);  
            invokeAll(subTareaA, subTareaB);
            try {
                combinarResultados(subTareaA.get(), subTareaB.get());
            } catch (InterruptedException ex) {
                Logger.getLogger(CalcularCapacidades.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(CalcularCapacidades.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            resolver();
        }
        
        return resultado;
    }
    
    private void resolver() {
        for (int i=inicio; i<fin; i++) {
            resultado.add(fotogramas[i].getNecesidad());
        }
    }
    
    private void combinarResultados(Info info1, Info info2) {
        for (TipoCapacidadGrafica capacidad: Constantes.TipoCapacidadGrafica.values()) {
            resultado.add(capacidad, info1.getInfo(capacidad));
            resultado.add(capacidad, info2.getInfo(capacidad));
        }
    }
}
