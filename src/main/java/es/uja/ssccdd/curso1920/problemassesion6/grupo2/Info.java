/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo2;

import es.uja.ssccdd.curso1920.problemassesion6.grupo2.Constantes.TipoCapacidadGrafica;

/**
 * Resultado del proceso.
 * @author fconde
 */
public class Info {
    
    private final int[] info;
    
    public Info() {
        this.info = new int[TipoCapacidadGrafica.values().length];
    }
    
    public void add(TipoCapacidadGrafica capacidad) {
        info[capacidad.ordinal()]++;
    }

    public void add(TipoCapacidadGrafica capacidad, int cantidad) {
        info[capacidad.ordinal()] += cantidad;
    }
    
    public int getInfo(TipoCapacidadGrafica capacidad) {
        return info[capacidad.ordinal()];
    }

    @Override
    public String toString() {
        String salida = "Info(\n";
        
        for (TipoCapacidadGrafica capacidad: TipoCapacidadGrafica.values()) {
            salida = salida + capacidad + "=" + info[capacidad.ordinal()] + "\n";
        }
        return salida;
    }
}
