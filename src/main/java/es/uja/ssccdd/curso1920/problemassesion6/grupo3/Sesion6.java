/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.OCURRENCIAS;
import es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.TipoProceso;
import java.util.concurrent.ForkJoinPool;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.TOTAL_PROCESOS;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author pedroj
 */
public class Sesion6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Proceso[] procesos;
        ForkJoinPool ejecucion;
        TipoProceso valor;
        Resultado resultado;
        BuscarProceso selector;
        

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucion = new ForkJoinPool();
        
        // Creamos el conjunto de búsqueda de forma aleatoria
        procesos = new Proceso[TOTAL_PROCESOS];
        for( int i = 0; i < TOTAL_PROCESOS; i++ )
            procesos[i] = new Proceso(i, TipoProceso.getProceso());
        
        // Se busca un número de ocurrencias para un tipo de proceso
        valor = TipoProceso.getProceso();
        System.out.println("Hilo(PRINCIPAL) comienza la búsqueda de " + OCURRENCIAS +
                            " procesos del tipo " + valor);
        selector = new BuscarProceso(procesos,INICIO,TOTAL_PROCESOS,valor);
        
        // Esperamos que el selector nos devuelva el resultado calculado
        resultado = ejecucion.invoke(selector);
        
        // Finalizamos el marco de ejecución
        ejecucion.shutdown();
        
        // Presentamos el inventario calculado
        System.out.println("HILO(Principal) el resultado de la búsqueda es \n______________\n" +
                            resultado + "\n______________");
        
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
