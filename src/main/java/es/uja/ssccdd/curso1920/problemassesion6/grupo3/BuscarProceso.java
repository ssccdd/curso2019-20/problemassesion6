/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo3;

import es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.TipoProceso;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo3.Constantes.VALOR_BASE;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author pedroj
 */
public class BuscarProceso extends RecursiveTask<Resultado> {
    private final Proceso[] procesos;
    private final int inicio;
    private final int fin;
    private final TipoProceso valor;

    public BuscarProceso(Proceso[] procesos, int inicio, int fin, TipoProceso valor) {
        this.procesos = procesos;
        this.inicio = inicio;
        this.fin = fin;
        this.valor = valor;
    }

    @Override
    protected Resultado compute() {
        Resultado resultado = null;
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                           " El espacio de búsqueda asignado, inicio " + inicio +
                           " fin " + fin);
        
        try {
            if( (fin - inicio) > VALOR_BASE )
                // Dividimos el problema a la mitad y esperamos a que finalicen
                resultado = dividir();
            else
                // Calculamos el problema base
                resultado = buscar();
        
        } catch (InterruptedException | ExecutionException ex) {
                System.out.println("HILO-" + Thread.currentThread().getName() + 
                           " CANCELADA la búsqueda asignada, inicio " + inicio +
                           " fin " + fin);
        }
        
        return resultado;
    }
    
    /**
     * Añadimos las posiciones encontradas al resultado, si se alcanza el valor
     * de ocurrencias esperado se solicita la interrupción del resto de tareas
     * activas.
     * @return
     *      Resultado de la búsqueda
     */
    private Resultado buscar() {
        Resultado resultado = new Resultado(valor);
        
        for( int i = inicio; i < fin; i++ )
            if( procesos[i].getProceso().equals(valor) )
                resultado.addPosicion(i);
        
        return resultado;
    }
    
    /**
     * Dividimo es espacio de búsqueda por la mitad y se le asigna a una tarea
     * de búsqueda. Mezclamos el resultado obtenido
     * @return 
     *      Resultado de la búsqueda
     */
    private Resultado dividir() throws InterruptedException, ExecutionException {
        Resultado resultado;
        
        int mitad = (inicio + fin) / 2;
        
        BuscarProceso tarea1 = new BuscarProceso(procesos,inicio,mitad,valor);
        BuscarProceso tarea2 = new BuscarProceso(procesos,mitad,fin,valor);
        
        // Añadimos las tareas de búsqueda al ejecutor
        invokeAll(tarea1,tarea2);
        
        // Mezclamos el resultado de las dos tareas
        resultado = mezcla(tarea1.get(), tarea2.get());
        
        return resultado;
    }
    
    private Resultado mezcla( Resultado res1, Resultado res2 ) {
        Resultado resultado = res1;
        ArrayList<Integer> listaPosiciones = res2.getPosicion();
        
        for( Integer pos : listaPosiciones )
            resultado.addPosicion(pos);
        
        return resultado;
    }
}
