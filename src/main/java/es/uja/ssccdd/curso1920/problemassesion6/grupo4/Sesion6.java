/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.INVENTARIO;
import es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.TipoSensor;
import java.util.concurrent.ForkJoinPool;

/**
 *
 * @author pedroj
 */
public class Sesion6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Sensor[] almacen;
        Inventario inventario;
        ForkJoinPool ejecucion;
        CalcularInventario operario;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucion = new ForkJoinPool();
        
        // Creamos los sensores del almacén de forma aleatoria
        almacen = new Sensor[INVENTARIO];
        for( int i = 0; i < INVENTARIO; i++ )
            almacen[i] = new Sensor(i, TipoSensor.getSensor());
        
        // El operario realiza el cálculo del inventario
        System.out.println("Hilo(PRINCIPAL) comienza el cálculo del inventario");
        inventario = new Inventario();
        operario = new CalcularInventario(almacen, INICIO, INVENTARIO, inventario);

        // Esperamos a que el operario finalice el inventario
        ejecucion.invoke(operario);
        
        // Finalizamos el marco de ejecución
        ejecucion.shutdown();
        
        // Presentamos el inventario calculado
        System.out.println("HILO(Principal) el inventario calculado es \n______________\n" +
                            inventario + "\n______________");
        
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
