/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.VALOR_BASE;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author pedroj
 */
public class CalcularInventario extends RecursiveAction {
    private final Sensor[] almacen;
    private final int inicio;
    private final int fin;
    private final Inventario inventario;

    public CalcularInventario(Sensor[] almacen, int inicio, int fin, 
                                                        Inventario inventario) {
        this.almacen = almacen;
        this.inicio = inicio;
        this.fin = fin;
        this.inventario = inventario;
    }
    
    @Override
    protected void compute() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                           " El espacio asignado del almacen, inicio " + inicio +
                           " fin " + fin);
        
        // Comprobamos si estamos en el caso base
        if( (fin - inicio) > VALOR_BASE ) {
            // Dividimos el problema a la mitad y esperamos a que finalicen
            int mitad = (inicio + fin) / 2;
            CalcularInventario tarea1 = new CalcularInventario(almacen, inicio, mitad,
                                                                inventario);
            CalcularInventario tarea2 = new CalcularInventario(almacen, mitad, fin,
                                                                inventario);
            // Esperamos a que finalicen las tareas
            invokeAll(tarea1,tarea2);
        } else {
            resolver();
        }
    }
    
    private void resolver() {
        for( int i = inicio; i < fin; i++ )
            inventario.add(almacen[i].getSensor());
    }
}
