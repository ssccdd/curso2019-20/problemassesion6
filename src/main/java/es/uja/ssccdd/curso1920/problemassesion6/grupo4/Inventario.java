/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion6.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.SENSORES;
import es.uja.ssccdd.curso1920.problemassesion6.grupo4.Constantes.TipoSensor;

/**
 *
 * @author pedroj
 */
public class Inventario {
    private final int[] inventario;

    /**
     * Inicializa el inventario para cada uno de los componentes disponibles
     * para la fabricación de un ordenador
     */
    public Inventario() {
        this.inventario = new int[SENSORES.length];
        for( int i = 0; i < SENSORES.length; i++ )
            inventario[i] = 0;
    }
    
    public synchronized void add(TipoSensor sensor) {
        inventario[sensor.ordinal()]++;
    }
    
    public synchronized void add(TipoSensor sensor, int cantidad) {
        inventario[sensor.ordinal()] += cantidad;
    }
    
    public synchronized void drop(TipoSensor sensor) {
        inventario[sensor.ordinal()]--;
    }
    
    public synchronized void drop(TipoSensor sensor, int cantidad) {
        inventario[sensor.ordinal()] -= cantidad;
    }
    
    public synchronized int getInventario(TipoSensor componente) {
        return inventario[componente.ordinal()];
    }

    @Override
    public synchronized String toString() {
        String salida = "Inventario{ Sensores\n\t";
        
        for( TipoSensor sensor : SENSORES )
            salida = salida + sensor + " = " 
                            + inventario[sensor.ordinal()] + " unidades\n\t";
        
        return salida + "}";
    }
}
